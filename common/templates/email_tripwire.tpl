To: $EMAIL_ADMIN
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: =?utf-8?Q?[$RELEASE_ACRONYM]: Dangerous packages detected: automation stopped =F0=9F=9B=91?=

Dear admins,

********************************
***  Manual action required  ***
********************************

The following dangerous packages have been detected in today's snapshot:

$PACKAGES

A .freeze.${RELEASE}all file has been created and links will not be updated.

Please see the following page for more information:

https://linuxops.web.cern.ch/distributions/snapshots/#-release-packages-the-dangerous-rpms

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)
