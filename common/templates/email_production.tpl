To: $EMAIL_USERS
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: [SECURITY]: $RELEASE_ACRONYM - $SHORTLIST [...]

Dear Linux users,

Today's $FULL_OS_NAME ($RELEASE_ACRONYM) system update contains the following packages:

--PACKAGES--

fixing multiple security vulnerabilities and/or providing bugfixes and enhancements.

For more information about vulnerabilities fixed please check:

 $WEBSITE/updates/$LONG_RELEASE/prod/latest_updates

This update can also be applied before nightly automated update run,
by running as root on your machine:

 # /usr/bin/dnf -y update

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)
