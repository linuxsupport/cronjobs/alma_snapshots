<div class="admonition news_date">${TODAY_FORMAT}</div>

## ${RELEASE_NAME} is available in ${CHANNEL}

We are pleased to announce that ${RELEASE_NAME} is available in ${CHANNEL} as of today.

The list of updated packages can be found [here](/${REPO_DEST_PATH}/${YEAR_MONTH}/monthly_updates/#${TODAY_FORMAT}).
