## :alma: ${RELEASE_NAME} is available in ${CHANNEL}

###### We are pleased to announce that ${RELEASE_NAME} is available in ${CHANNEL} as of today.

###### The list of updated packages can be found [here](${WEBSITE}/${REPO_DEST_PATH}/${YEAR_MONTH}/monthly_updates/#${TODAY_FORMAT}).

(Please note that the website may take about 20 minutes to be updated)
