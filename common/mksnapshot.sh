#!/bin/bash

source /root/common.sh
source /root/common_functions.sh

echo "Creating snapshot for $TODAY"

# Clean up rsync temporary files
cleanUp() {
  find $1 -name '.~tmp~' -exec rm -rv {} \; 2>/dev/null
}

# If we're going to kludge a repository, save a clean version first.
kludgeBackup() {
  local REPO="$DEST/$1"
  local BACKUP="`dirname $DEST/.clean.$1`"

  [[ -d "$BACKUP" ]] && return

  mkdir -pv $BACKUP
  cp -Rl $REPO $BACKUP
}

# Look for packages in our filter list and remove them if necessary.
# Grab packages from our include list and add them to the snapshot.
kludgeRepository() {
  local REPO="$DEST/$1"
  local PREV="$SNAPS/$YESTERDAY/$1"

  local DIRTY=0
  while IFS= read -r RPM; do
    [[ -z "${RPM}" ]] && continue
    local FILE="`find "${REPO}" -name "${RPM}"`"

    if [[ -n "${FILE}" ]]; then
      # Back up what we have before we screw with it
      kludgeBackup $1

      echo "Found ${RPM} at ${FILE}"
      echo "${FILE}" | xargs rm -v
      DIRTY=1
    else
      echo "No matches for: find "${REPO}" -name "${RPM}""
    fi
  done <<< "`sed 's/^\s*//;s/\s*$//;s/\s*#.*$//;/^\s*$/d' /root/packages_filtered.lst`"
  # Sed cleans up our list of filtered packages by removing leading/trailing spaces, comments and empty lines

  while IFS= read -r LINE; do
    [[ -z "$LINE" ]] && continue
    local LINE="${LINE//\$ARCH/$ARCH}"
    local repo="`echo "${LINE}" | cut -d ';' -f 1`"

    # Check what repo we're in, stripping final slashes just in case
    [[ "${1%/}" != "${repo%/}" ]] && continue

    local RPM="`echo "${LINE}" | cut -d ';' -f 2`"
    echo "Looking to include ${RPM}"

    local dir="`dirname "/data/${RPM}"`"
    local filename="`basename "/data/${RPM}"`"

    local FILE="`find "$dir" -name "$filename"`"

    if [[ -n "${FILE}" ]]; then
      # Back up what we have before we screw with it
      kludgeBackup $1

      echo "Found ${FILE}"
      echo "${FILE}" | xargs -I{} cp -vl {} $REPO/Packages/
      DIRTY=1
    fi
  done <<< "`sed 's/^\s*//;s/\s*$//;s/\s*#.*$//;/^\s*$/d' /root/packages_included.lst`"

  # If we modified something, we need to regenerate the repodata
  if [[ $DIRTY -eq 1 ]]; then
    # Look for comps files, we need them for the new metadata
    COMPS=`find "${REPO}" -name '*-comps-*.xml' -not -path '*.~tmp~*' -print -quit`
    if [[ -n "${COMPS}" ]]; then
      COMPS="-g ${COMPS}"
    fi

    # We're about the change the repodata, so the signature won't be valid anymore
    rm -vf "${REPO}/repodata/repomd.xml.asc"

    echo "Packages kludged, regenerating metadata"
    /usr/bin/createrepo \
      --workers 5 \
      --xz \
      --update \
      --keep-all-metadata \
      $COMPS \
      --outputdir $REPO \
      $REPO

    # Now let's see which files haven't changed and we can hard-link again
    while IFS= read -r FILE; do
      if cmp --silent "${REPO}/repodata/${FILE}" "${PREV}/repodata/${FILE}"; then
        echo "${FILE} has not changed, hard-linking to previous version"
        cp -fl "${PREV}/repodata/${FILE}" "${REPO}/repodata/${FILE}"
      fi
    done <<< "`find ${REPO}/repodata/ -type f -printf '%f\n'`"
  fi
}

# Make sure we have repo metadata, and create it if necessary
checkRepodata() {
  local REPO="$DEST/$1"
  local PREV="$SNAPS/$YESTERDAY/$1"
  local DIFF="${2:-$REPO/.diff}"

  # Check if our repodata exists and is valid
  if ! repoquery --refresh --repoid test --repofrompath=test,$REPO list available >/dev/null 2>&1; then
    # Just in case we have a partial repodata, we need to remove it
    rm -rfv $REPO/repodata
    if [[ -s "$DIFF" || ! -d "$PREV/repodata" ]]; then
      mkdir -p $REPO
      # Something changed, so we need to generate new repodata
      /usr/bin/createrepo \
        --workers 5 \
        --xz \
        --outputdir $REPO \
        $REPO
    else
      # No changes, just hardlink the previous repodata
      cp -Rl $PREV/repodata $REPO
    fi
  fi
}

# Make sure we start from a clean slate
quickDelete $TODELETE
quickDelete $DEST
mkdir -pv $DEST
echo "$TODAY" > $DEST/snapshotdate.txt
jq -r '.payload.compose.id' $SOURCE/metadata/${SUPPORTED_ARCHES%% *}/composeinfo.json > $DEST/COMPOSE_ID

for REPO in $UPSTREAM_REPOS; do
  for ARCH in $SUPPORTED_ARCHES; do
    # Create the new path
    mkdir -pv $DEST/$REPO/$ARCH/os

    # Copy stuff
    if [[ "$REPO" == "testing" ]]; then
      cp -Rl $VAULT/$REPO/$ARCH/os/{Packages,repodata} $DEST/$REPO/$ARCH/os/
    else
      cp -Rl $SOURCE/$REPO/$ARCH/os/{Packages,repodata} $DEST/$REPO/$ARCH/os/
      cp -Rl $SOURCE/$REPO/$ARCH/os/.{disc,tree}info $DEST/$REPO/$ARCH/os/ 2>/dev/null
    fi

    # We also need the images directory of BaseOS
    if [[ "$REPO" == "BaseOS" ]]; then
      cp -Rl $SOURCE/$REPO/$ARCH/os/images $DEST/$REPO/$ARCH/os/
    fi

    # Clean up sync artifacts
    cleanUp $DEST/$REPO/$ARCH/os/

    # Remove filtered packages
    kludgeRepository $REPO/$ARCH/os/

    # Look for differences
    diffRepos $DEST/$REPO/$ARCH/os/Packages $SNAPS/$YESTERDAY/$REPO/$ARCH/os/Packages > $DEST/$REPO/$ARCH/os/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$REPO/$ARCH/os/.diff" ] || rm -f "$DEST/$REPO/$ARCH/os/.diff"

    # If we don't have a repodata (should never happen), we will have to create it
    checkRepodata "$REPO/$ARCH/os"

    # Now we copy the debuginfo RPMs
    mkdir -pv $DEST/$REPO/$ARCH/debug
    cp -Rl $VAULT/$REPO/debug/$ARCH/* $DEST/$REPO/$ARCH/debug/
    cleanUp $DEST/$REPO/$ARCH/debug
    diffRepos $DEST/$REPO/$ARCH/debug/Packages $SNAPS/$YESTERDAY/$REPO/$ARCH/debug/Packages > $DEST/$REPO/$ARCH/debug/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$REPO/$ARCH/debug/.diff" ] || rm -f "$DEST/$REPO/$ARCH/debug/.diff"

    # If we don't have a repodata (should never happen), we will have to create it
    checkRepodata "$REPO/$ARCH/debug"

    # If we already have a source directory, it's because we copied it for a previous $ARCH, so we can continue
    [[ -d $DEST/$REPO/source ]] && continue

    # Copy sources
    mkdir -pv $DEST/$REPO/source
    cp -Rl $VAULT/$REPO/Source/* $DEST/$REPO/source/
    cleanUp $DEST/$REPO/source
    diffRepos $DEST/$REPO/source/Packages $SNAPS/$YESTERDAY/$REPO/source/Packages > $DEST/$REPO/source/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$REPO/source/.diff" ] || rm -f "$DEST/$REPO/source/.diff"

    # If we don't have a repodata (should never happen), we will have to create it
    checkRepodata "$REPO/source"
  done
done

# Now we copy the Koji RPMs
for REPO in $KOJI_REPOS; do
  snapshotKojiRepo $REPO
done

# Copy the SIG content (TODO: this will need to be reviewed)
for REPO in $SIG_REPOS; do
  for ARCH in $SUPPORTED_ARCHES; do
    # Create the new path
    mkdir -pv $DEST/$REPO/source

    for PROJECT in $(ls $SIG_RPMS/$REPO/$ARCH/); do
      # Create the new path
      mkdir -pv $DEST/$REPO/$ARCH/$PROJECT/

      # Copy stuff
      cp -Rl $SIG_RPMS/$REPO/$ARCH/$PROJECT/{Packages,repodata,debug} $DEST/$REPO/$ARCH/$PROJECT/

      # Clean up sync artifacts
      cleanUp $DEST/$REPO/$ARCH/$PROJECT/

      # Remove filtered packages
      kludgeRepository $REPO/$ARCH/$PROJECT/

      # Look for differences
      diffRepos $DEST/$REPO/$ARCH/$PROJECT/Packages $SNAPS/$YESTERDAY/$REPO/$ARCH/$PROJECT/Packages >> $DEST/$REPO/$ARCH/.diff

      # If we don't have a repodata (should never happen), we will have to create it
      checkRepodata "$REPO/$ARCH/$PROJECT" "$REPO/$ARCH/.diff"
      checkRepodata "$REPO/$ARCH/$PROJECT/debug"

      # If we already have a source directory, it's because we copied it for a previous $ARCH, so we can continue
      [[ -d $DEST/$REPO/source/$PROJECT ]] && continue

      # Copy sources
      cp -Rl $SIG_RPMS/$REPO/source/$PROJECT $DEST/$REPO/source/
      cleanUp $DEST/$REPO/source/
      diffRepos $DEST/$REPO/source/$PROJECT/Packages $SNAPS/$YESTERDAY/$REPO/source/$PROJECT/Packages >> $DEST/$REPO/source/.diff

      # If there's no diff, just delete the file
      [ -s "$DEST/$REPO/source/.diff" ] || rm -f "$DEST/$REPO/source/.diff"

      # If we don't have a repodata (which shouldn't happen), we will have to create it
      checkRepodata "$REPO/source/$PROJECT"
    done

    # If there's no diff, just delete the file
    [ -s "$DEST/$REPO/$ARCH/.diff" ] || rm -f "$DEST/$REPO/$ARCH/.diff"

  done
done

# Point .*-latest to today's snapshot
cd $DESTINATION
linkTargetTo $SNAPS_DIR/.tmp.$TODAY ".${RELEASE}-latest"
/root/regen-repos.sh "${LONG_RELEASE}-latest-.*"

echo "Sending Daily diff"

/root/sendemail.sh $DEST "daily"
