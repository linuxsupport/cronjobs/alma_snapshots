addDays() {
  DAYS=${2:-1} # default to 1 day
  /bin/date +%Y%m%d --date="$1+$DAYS days"
}

diffRepos() {
  NEW=$1
  OLD=$2

  for f in $(/usr/bin/diff --new-line-format="" --unchanged-line-format="" \
      <(find $NEW -type f -iname "*.rpm" -printf "%f\n" | sort)  \
      <(find $OLD -type f -iname "*.rpm" -printf "%f\n" | sort)); do
    # We compare by rpm filename, but we need the full path to get the rpm metadata
    path=$(find $NEW -iname "$f" -printf "%P\n")
    rpm -qp --queryformat "$f;%{name};%{version};%{release};%{arch};%{sourcerpm};%{buildtime}\n" $NEW/$path 2>/dev/null
  done

}

snapshotKojiRepo() {
  REPO=$1
  for ARCH in $SUPPORTED_ARCHES; do
    NAME="${REPO%%${KOJI_TAG_SUFFIX}}"
    if [[ $NAME == "cern" ]]; then
      NAME="CERN"
    fi

    # Create the new path
    mkdir -pv $DEST/$NAME/$ARCH

    # Copy the RPMs
    cp -Rl $KOJI_RPMS/$REPO/$ARCH/os/Packages $DEST/$NAME/$ARCH
    cp -Rl $KOJI_RPMS/$REPO/$ARCH/os/repodata $DEST/$NAME/$ARCH

    # Look for differences
    diffRepos $DEST/$NAME/$ARCH/Packages $SNAPS/$YESTERDAY/$NAME/$ARCH/Packages > $DEST/$NAME/$ARCH/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$NAME/$ARCH/.diff" ] || rm -f "$DEST/$NAME/$ARCH/.diff"

    # If we already have a Source directory, it's because we copied it for a previous $ARCH, so we can continue
    [[ -d $DEST/$NAME/Source ]] && continue

    # Copy sources (as 'Source' to match upstream repos)
    mkdir -pv $DEST/$NAME/Source
    cp -Rl $KOJI_RPMS/$REPO/source/SRPMS/* $DEST/$NAME/Source
    diffRepos $DEST/$NAME/Source $SNAPS/$YESTERDAY/$NAME/Source > $DEST/$NAME/Source/.diff

    # If there's no diff, just delete the file
    [ -s "$DEST/$NAME/Source/.diff" ] || rm -f "$DEST/$NAME/Source/.diff"
  done
}

# Try to delete faster than just a straight `rm -rf`
quickDelete() {
  [ -e $1 ] || return 0
  echo "Deleting $1"
  time for d in `find $1 -depth -type d 2>/dev/null`; do
    pushd $d >/dev/null
    perl -e 'for(<*>){unlink}'
    popd >/dev/null
    rm -rf $d
  done
  rm -rfv $1
}

template() {
  local TEMPLATE=/root/templates/$1
  shift

  env \
    RELEASE="$RELEASE" \
    RELEASE_ACRONYM="${LONG_RELEASE^^}" \
    FULL_OS_NAME="$FULL_OS_NAME" \
    LONG_RELEASE="$LONG_RELEASE" \
    TODAY="$TODAY" \
    WEBSITE="$WEBSITE" \
    "$@" \
    envsubst < $TEMPLATE
}

cleanDisttag() {
  # Given a RPM's version-release, return the version-release without the disttag
  # e.g. 1.2.3-4.el8 -> 1.2.3-4
  NVR=${1:-$(</dev/stdin)}
  NVR=${NVR%.cern}
  NVR=${NVR%.el${RELEASE}}
  NVR=${NVR%.al${RELEASE}}
  echo $NVR
}

linkTargetTo() {
    # $LINK -> $TARGET
    local TARGET=$1
    local LINK=$2

    # If $TARGET is a symlink, find out what it ultimately points to and link to that instead.
    # There's a limit to how many symlinks can point to each other. The limit, of course, is 42.
    # We shouldn't hit it, but it's been known to happen...
    [[ -h $TARGET ]] && TARGET=$(realpath $TARGET --relative-to=$(dirname $LINK))

    ln -vfsT $TARGET $LINK
}
