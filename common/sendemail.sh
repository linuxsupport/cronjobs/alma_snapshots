#!/bin/bash

source /root/common.sh
source /root/common_functions.sh

TMPDIR=$1
if [[ $2 == "daily" ]]; then
  CHANNEL="daily"
  TEMPLATE="email_dailydiff.tpl"
  EMAIL_TO="$EMAIL_ADMIN"
elif [[ $2 == $LN_TESTING ]]; then
  CHANNEL="testing"
  NEW_RELEASE_FILE=".${RELEASE}-testing-new-release"
  TEMPLATE="email_testing.tpl"
  EMAIL_TO="$EMAIL_USERS_TEST"
else
  CHANNEL="production"
  NEW_RELEASE_FILE=".${RELEASE}-new-release"
  TEMPLATE="email_production.tpl"
  EMAIL_TO="$EMAIL_USERS"
fi


for ARCH in $SUPPORTED_ARCHES; do
  for REPO in $ALL_REPOS; do
    CLEANREPO="${REPO/\//-}"
    if [[ $CHANNEL == "daily" ]]; then
      SEARCH="-path '*/$REPO/$ARCH/*' -name .diff"
    elif [[ $CHANNEL == "testing" ]]; then
      SEARCH="-name '.diff:*-$LN_TESTING:$CLEANREPO:$ARCH'"
    else
      SEARCH="-name '.diff:*:$CLEANREPO:$ARCH' -not -name '.diff:*-$LN_TESTING:*'"
    fi
    eval find "$TMPDIR" $SEARCH -exec cat {} '\;' | sort -fuo "$TMPDIR/.diff:$CLEANREPO:$ARCH"

    # Check if .diff file is empty
    if [[ ! -s "$TMPDIR/.diff:$CLEANREPO:$ARCH" ]]; then
      rm -f "$TMPDIR/.diff:$CLEANREPO:$ARCH"
      continue
    fi

    cat "$TMPDIR/.diff:$CLEANREPO:$ARCH" >> "$TMPDIR/.longlist"

    # Log the diff for posterity
    awk -F';' -v REPO=$REPO -v ARCH=$ARCH '{print "Repodiff: "REPO" "ARCH": "$2" "$3" "$4" "$5}' "$TMPDIR/.diff:$CLEANREPO:$ARCH"
  done
done

STATE=""
while IFS= read -r LINE; do
  MODTIME="`echo $LINE | cut -d'.' -f1`"
  LINK="`echo $LINE | cut -d' ' -f2`"
  TARGET="`readlink $DESTINATION/$LINK`"
  STATE="$STATE  `printf '%-20s' "$LINK"` -> $TARGET (modified $MODTIME)\n"
done <<< "`find $DESTINATION -maxdepth 1 -type l -name \"${RELEASE}*\" -printf '%T+ %P\n' | sort | tail -n 10`"
# grab the last 10 symlinks so we can see the state of the previous point release as well

FROZEN=`find $DESTINATION -maxdepth 1 -type f -name ".freeze.${RELEASE}*" -print -quit`
if [[ -n "$FROZEN" ]]; then
  STATE="$STATE\n *******************************************************\n"
  STATE="$STATE ***  .freeze.${RELEASE}* exists, links won't be updated  ***\n"
  STATE="$STATE *******************************************************\n"
fi
STATE="`printf "$STATE"`"

# Let's filter .longlist first
cleanDiff "$TMPDIR/.longlist" > "$TMPDIR/.longlist.2"
# Now keep just the package names, remove duplicates and delete the temp file
cut -d';' -f2 "$TMPDIR/.longlist.2" | sort -fu > "$TMPDIR/.longlist"
rm "$TMPDIR/.longlist.2"

# There are no diffs to report on, we can stop
if [[ ! -s "$TMPDIR/.longlist" ]]; then
  if [[ $CHANNEL == "daily" ]]; then
    NOPKG_SUBJECT_PREFIX="[DIFF]"
  elif [[ $CHANNEL == "testing" ]]; then
    NOPKG_SUBJECT_PREFIX="[TEST]"
  else
    NOPKG_SUBJECT_PREFIX="[SECURITY]"
  fi
  # But let's inform the admins (so they don't worry unneccessarily)
  template email_nopackages.tpl NOPKG_SUBJECT_PREFIX="$NOPKG_SUBJECT_PREFIX" STATE="$STATE" >> "$TMPDIR/.email"
  cat "$TMPDIR/.email" | swaks --server $MAILMX --to $EMAIL_ADMIN --data -
  rm "$TMPDIR/.email"
  exit
fi

# Update website and the mattermost channel
if [[ $CHANNEL == "production" ]] || [[ $CHANNEL == "testing" ]]; then
  /root/commit-updates-to-docs.sh $TMPDIR $CHANNEL
  /root/notifymm.sh $TMPDIR $CHANNEL
fi

if [[ -f "$TMPDIR/${NEW_RELEASE_FILE}" ]]; then
  # We have a new release, so that's the highlight for the subject line
  NEW_RELEASE_DETECTED=$(cat "$TMPDIR/${NEW_RELEASE_FILE}")
  SHORTLIST="${FULL_OS_NAME% $RELEASE} $NEW_RELEASE_DETECTED released" # ie. 'Red Hat Enterprise Linux 9.2 released'
else
  # No new release. Lets assemble the list of highlights for the email's subject
  SHORTLIST=`grep -f <(echo -n "$HIGHLIGHT_RPMS" | sed 's/\([^[:space:]]\+\) \?/^\1$\n/g') "$TMPDIR/.longlist" | head -n $SHORTLIST_COUNT | xargs`
  LEN_SHORTLIST=`echo "$SHORTLIST" | awk -F" " '{printf "%d",NF}'`
  if [[ $LEN_SHORTLIST -lt $SHORTLIST_COUNT ]]; then
    D=`expr $SHORTLIST_COUNT - $LEN_SHORTLIST`
    SHORTLIST="$SHORTLIST `grep -v -f <(echo -n "$HIGHLIGHT_RPMS" | sed 's/\([^[:space:]]\+\) \?/^\1$\n/g') "$TMPDIR/.longlist" | head -n $D | xargs`"
  fi
  SHORTLIST="`echo "$SHORTLIST" | sed 's/\b\s\+\b/, /g'`"
fi
rm "$TMPDIR/.longlist"

echo "Sending email to $CHANNEL users"

template $TEMPLATE SHORTLIST="$SHORTLIST" STATE="$STATE" | sed '/^--PACKAGES--$/Q' > "$TMPDIR/.email"
for ARCH in $SUPPORTED_ARCHES; do
  for REPO in $ALL_REPOS; do
    CLEANREPO="${REPO/\//-}"
    [ -e "$TMPDIR/.diff:$CLEANREPO:$ARCH" ] || continue

    echo "$REPO $ARCH repository:" >> "$TMPDIR/.email"

    # Let's assemble the summary of packages for this repo
    cleanDiff "$TMPDIR/.diff:$CLEANREPO:$ARCH" > "$TMPDIR/.pkglist"
    # Truncate the list to at most $MAX_PACKAGES_LIST lines
    sed -i ${MAX_PACKAGES_LIST}q "$TMPDIR/.pkglist"

    # Print it nicely formatted in the email
    awk -F';' '{printf "  %-50s %s-%s\n", $2, $3, $4}' "$TMPDIR/.pkglist" >> "$TMPDIR/.email"

    LINES=$(wc -l "$TMPDIR/.diff:$CLEANREPO:$ARCH" | cut -d' ' -f1)
    AFTER=$(wc -l "$TMPDIR/.pkglist" | cut -d' ' -f1)
    if [[ $LINES -gt $AFTER ]]; then
      echo "   [... truncated list (${LINES} packages in total) ...]" >> "$TMPDIR/.email"
    fi
    echo >> "$TMPDIR/.email"
    rm "$TMPDIR/.pkglist"

  done
done
template $TEMPLATE SHORTLIST="$SHORTLIST" STATE="$STATE" | sed '1,/^--PACKAGES--$/d' >> "$TMPDIR/.email"
cat "$TMPDIR/.email" | swaks --server $MAILMX --to $EMAIL_TO --data -
rm "$TMPDIR/.email"
