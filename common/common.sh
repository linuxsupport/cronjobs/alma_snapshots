## Basic config file
LONG_RELEASE="alma${RELEASE}"
FULL_OS_NAME="AlmaLinux ${RELEASE}"
RELEASE_RPM="almalinux-release"
KOJI_TAG_SUFFIX="${RELEASE}al-stable"
TODAY=`/bin/date +%Y%m%d`
TODAY_FORMAT=`/bin/date +%Y-%m-%d`
YESTERDAY=`/bin/date +%Y%m%d --date='yesterday'`
DELAYED=`/bin/date +%Y%m%d --date="$PRODDATE"`
TOO_OLD=`/bin/date +%Y%m%d --date="$OLDESTDATE"`

SOURCE="/data/almalinux/${RELEASE}"
VAULT="/data/almalinux-vault/${RELEASE}"
SIG_RPMS="/data/centos-stream/SIGs/9-stream"
DESTINATION="/data/$PATH_DESTINATION"
KOJI_RPMS="/data/internal/repos"
SNAPS_DIR="${RELEASE}-snapshots"
SNAPS="$DESTINATION/$SNAPS_DIR"
DEST=$SNAPS/.tmp.$TODAY
TODELETE=$SNAPS/.todelete.$TODAY
FINALDEST=$SNAPS/$TODAY

UPSTREAM_REPOS="BaseOS AppStream HighAvailability RT ResilientStorage CRB NFV devel plus extras synergy testing"
KOJI_REPOS="cern${KOJI_TAG_SUFFIX} openafs${KOJI_TAG_SUFFIX}"
SIG_REPOS="cloud nfv"
SUPPORTED_ARCHES="x86_64 aarch64"
ALL_REPOS="CERN openafs $UPSTREAM_REPOS"

LN_TESTING="testing"
WEBSITE="https://linux.cern.ch"
MAILMX="cernmx.cern.ch"

SHORTLIST_COUNT=4
MAX_PACKAGES_LIST=25
DANGEROUS_RPMS="$RELEASE_RPM almalinux-release-synergy almalinux-release-testing epel-release"
HIGHLIGHT_RPMS="$RELEASE_RPM kernel kernel-plus kernel-rt dbus glibc \
  cern-get-keytab hepix qemu-kvm systemd libvirt firewalld python sssd kmod-openafs"

ADVISORY_DB="${LONG_RELEASE^^}"
