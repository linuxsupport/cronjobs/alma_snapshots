# AlmaLinux snapshots

This repo is the source for alma{8,9}_snapshots.

Files common to both jobs go in the `common/` subdirectory, but they can be overridden
for each job by placing files in the `alma8/` or `alma9/` subdirectories.
